package cn.edu.dgut.sw.lab.security.oauth2.client;

import com.nimbusds.oauth2.sdk.*;
import com.nimbusds.oauth2.sdk.http.HTTPRequest;
import com.nimbusds.oauth2.sdk.http.HTTPResponse;
import com.nimbusds.oauth2.sdk.id.ClientID;
import net.minidev.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.oauth2.client.endpoint.OAuth2AccessTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.OAuth2Error;
import org.springframework.security.oauth2.core.OAuth2ErrorCodes;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;
import org.springframework.util.CollectionUtils;

import java.io.IOException;
import java.net.URI;
import java.util.*;

/**
 * @author Sai
 * @since 1.0
 *
 * 2018-9-29
 */
@Deprecated
public class DgutNimbusAuthorizationCodeTokenResponseClient implements OAuth2AccessTokenResponseClient<OAuth2AuthorizationCodeGrantRequest> {
    private static final String INVALID_TOKEN_RESPONSE_ERROR_CODE = "invalid_token_response";

    @SuppressWarnings("Duplicates")
    @Override
    public OAuth2AccessTokenResponse getTokenResponse(OAuth2AuthorizationCodeGrantRequest authorizationGrantRequest)
            throws OAuth2AuthenticationException {

        ClientRegistration clientRegistration = authorizationGrantRequest.getClientRegistration();

        // Build the authorization code grant request for the token endpoint
        AuthorizationCode authorizationCode = new AuthorizationCode(
                authorizationGrantRequest.getAuthorizationExchange().getAuthorizationResponse().getCode());
        URI redirectUri = toURI(authorizationGrantRequest.getAuthorizationExchange().getAuthorizationRequest().getRedirectUri());
        AuthorizationGrant authorizationCodeGrant = new AuthorizationCodeGrant(authorizationCode, redirectUri);
        URI tokenUri = toURI(clientRegistration.getProviderDetails().getTokenUri());

        // Set the credentials to authenticate the client at the token endpoint
        ClientID clientId = new ClientID(clientRegistration.getClientId());
//        Secret clientSecret = new Secret(clientRegistration.getClientSecret());
//
//        ClientAuthentication clientAuthentication;
//        if (ClientAuthenticationMethod.POST.equals(clientRegistration.getClientAuthenticationMethod())) {
//            clientAuthentication = new ClientSecretPost(clientId, clientSecret);
//        } else {
//            clientAuthentication = new ClientSecretBasic(clientId, clientSecret);
//        }

        // dgut的token接口使用的参数名为appid和appsecret，不是oauth2标准中的client_id，这里不作修改，直接增加参数。
        // 还需要token和userip
        Map<String, List<String>> customParams = new HashMap<>();
        customParams.put("token", Collections.singletonList(authorizationCode.getValue()));
        customParams.put("appid", Collections.singletonList(clientRegistration.getClientId()));
        customParams.put("appsecret", Collections.singletonList(clientRegistration.getClientSecret()));
        customParams.put("userip", Collections.singletonList((String) authorizationGrantRequest.getAuthorizationExchange().getAuthorizationRequest().getAdditionalParameters().get("ip")));

        com.nimbusds.oauth2.sdk.TokenResponse tokenResponse;
        try {
            // Send the Access Token request
            TokenRequest tokenRequest = new TokenRequest(tokenUri, clientId, authorizationCodeGrant, null, null, null, customParams);
            HTTPRequest httpRequest = tokenRequest.toHTTPRequest();
            httpRequest.setAccept(MediaType.APPLICATION_JSON_VALUE);
            httpRequest.setConnectTimeout(30000);
            httpRequest.setReadTimeout(30000);
            HTTPResponse httpResponse = httpRequest.send();

            // dgut的token接口返回的response的contentType为text/html;charset=utf-8,需要修改为application/josn，否则会抛异常。
            httpResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);
            httpResponse.ensureStatusCode(HTTPResponse.SC_OK);
            JSONObject jsonObject = httpResponse.getContentAsJSONObject();
            // dgut的token接口返回的json中没有token_type,需要增加一个，否则会抛异常。
            jsonObject.appendField("token_type", "Bearer");

            tokenResponse = com.nimbusds.oauth2.sdk.TokenResponse.parse(jsonObject);
        } catch (ParseException pe) {
            org.springframework.security.oauth2.core.OAuth2Error oauth2Error = new org.springframework.security.oauth2.core.OAuth2Error(INVALID_TOKEN_RESPONSE_ERROR_CODE,
                    "An error occurred parsing the Access Token response: " + pe.getMessage(), null);
            throw new OAuth2AuthenticationException(oauth2Error, oauth2Error.toString(), pe);
        } catch (IOException ioe) {
            throw new AuthenticationServiceException("An error occurred while sending the Access Token Request: " +
                    ioe.getMessage(), ioe);
        }

        if (!tokenResponse.indicatesSuccess()) {
            TokenErrorResponse tokenErrorResponse = (TokenErrorResponse) tokenResponse;
            ErrorObject errorObject = tokenErrorResponse.getErrorObject();
            org.springframework.security.oauth2.core.OAuth2Error oauth2Error;
            if (errorObject == null) {
                oauth2Error = new org.springframework.security.oauth2.core.OAuth2Error(OAuth2ErrorCodes.SERVER_ERROR);
            } else {
                oauth2Error = new OAuth2Error(
                        errorObject.getCode() != null ? errorObject.getCode() : OAuth2ErrorCodes.SERVER_ERROR,
                        errorObject.getDescription(),
                        errorObject.getURI() != null ? errorObject.getURI().toString() : null);
            }
            throw new OAuth2AuthenticationException(oauth2Error, oauth2Error.toString());
        }

        AccessTokenResponse accessTokenResponse = (AccessTokenResponse) tokenResponse;

        String accessToken = accessTokenResponse.getTokens().getAccessToken().getValue();
        OAuth2AccessToken.TokenType accessTokenType = null;
        if (OAuth2AccessToken.TokenType.BEARER.getValue().equalsIgnoreCase(accessTokenResponse.getTokens().getAccessToken().getType().getValue())) {
            accessTokenType = OAuth2AccessToken.TokenType.BEARER;
        }
        long expiresIn = accessTokenResponse.getTokens().getAccessToken().getLifetime();

        // As per spec, in section 5.1 Successful Access Token Response
        // https://tools.ietf.org/html/rfc6749#section-5.1
        // If AccessTokenResponse.scope is empty, then default to the scope
        // originally requested by the client in the Authorization Request
        Set<String> scopes;
        if (CollectionUtils.isEmpty(accessTokenResponse.getTokens().getAccessToken().getScope())) {
            scopes = new LinkedHashSet<>(
                    authorizationGrantRequest.getAuthorizationExchange().getAuthorizationRequest().getScopes());
        } else {
            scopes = new LinkedHashSet<>(
                    accessTokenResponse.getTokens().getAccessToken().getScope().toStringList());
        }
        scopes.add("dgut");

        String refreshToken = null;
        if (accessTokenResponse.getTokens().getRefreshToken() != null) {
            refreshToken = accessTokenResponse.getTokens().getRefreshToken().getValue();
        }

        Map<String, Object> additionalParameters = new LinkedHashMap<>(accessTokenResponse.getCustomParameters());

        return OAuth2AccessTokenResponse.withToken(accessToken)
                .tokenType(accessTokenType)
                .expiresIn(expiresIn)
                .scopes(scopes)
                .refreshToken(refreshToken)
                .additionalParameters(additionalParameters)
                .build();
    }

    private static URI toURI(String uriStr) {
        try {
            return new URI(uriStr);
        } catch (Exception ex) {
            throw new IllegalArgumentException("An error occurred parsing URI: " + uriStr, ex);
        }
    }
}
