package cn.edu.dgut.sw.lab.security.oauth2.client;

import org.springframework.security.oauth2.client.endpoint.NimbusAuthorizationCodeTokenResponseClient;
import org.springframework.security.oauth2.client.endpoint.OAuth2AuthorizationCodeGrantRequest;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.endpoint.OAuth2AccessTokenResponse;

@Deprecated
public class SaiNimbusAuthorizationCodeTokenResponseClientProxy extends NimbusAuthorizationCodeTokenResponseClient {

    private final WeiXinNimbusAuthorizationCodeTokenResponseClient wxClient = new WeiXinNimbusAuthorizationCodeTokenResponseClient();
    private final DgutNimbusAuthorizationCodeTokenResponseClient dgutClient = new DgutNimbusAuthorizationCodeTokenResponseClient();
    private final QQNimbusAuthorizationCodeTokenResponseClient qqClient = new QQNimbusAuthorizationCodeTokenResponseClient();

    @Override
    public OAuth2AccessTokenResponse getTokenResponse(OAuth2AuthorizationCodeGrantRequest authorizationGrantRequest) throws OAuth2AuthenticationException {

        switch (authorizationGrantRequest.getClientRegistration().getRegistrationId()) {
            case "dgut":
                return dgutClient.getTokenResponse(authorizationGrantRequest);
            case "weixin":
                return wxClient.getTokenResponse(authorizationGrantRequest);
            case "qq":
                return qqClient.getTokenResponse(authorizationGrantRequest);
            default:
                return super.getTokenResponse(authorizationGrantRequest);
        }
    }
}
